**Git** is an Open Source Distributed Version Control System.
Distributed Version Control System means Git has a remote repository which is stored in a server and a local repository which is stored in the computer of each developer. This means that the code is not just stored in a central server, but the full copy of the code is present in all the developers’ computers.
**Functions of Git :**

1. Repository:
   It is a collection of files and folders and helps in tracking the changes done by a specific user on one version of the repository.

2. Commit:
   A commit is simply a record of an “atomic” change - that is, a change which does not consist of multiple smaller changes.The commit will only exist on your local machine until it is pushed to a remote repository.

3. Fork:
   Forking is just containing a separate copy of the repository and there is no command involved.
4. Branch:
   It is simply a lightweight movable pointer to commits. The default branch name in Git is master. As you start making commits, you're given a master branch that points to the last commit you made. Every time you commit, the master branch pointer moves forward automatically.

5. Merge:
   Git merging combines sequences of commits into one unified history of commits. There are two main ways Git will merge: Fast Forward and Three way. Git can automatically merge commits unless there are changes that conflict in both commit sequences.
6. Pull:
   The git pull command is used to fetch and download content from a remote repository and immediately update the local repository to match that content. 
7. Push:
  The git push command is used to upload local repository content to a remote repository. Pushing is how you transfer commits from your local repository to a remote repo. 
8. Clone:
   Cloning is done through the command 'git clone' and it is a process of receiving all the code files to the local machine.