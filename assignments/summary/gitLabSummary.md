**Markdown**
Markdown is a lightweight and easy-to use syntax for styling all forms of writing on the GitHub Platform. What we learn from 

Markdown:

- format makes styled collabrative editing easy.
- differs from traditional formatting approaches.
- to format text.


Here are some of the attributes in the module:

Bold - To bold text, add two asteriks(*) before and after a word or phrase.

Italics - To italicize text, and underscore(_) before and after a word or phrase.

Bold and Italics - To emphasize text with bold and italics at same time, add three asteriks or underscores before and after a word or phrase.

Inline link - To create an inline link, use set of regular parenthesis immediately after the link text's closing square brackets.

Reference link

Paragraph - To create paragraphs use one or more linesconsecutive text followed by one or more blank lines.

Images link - To create an image link , use (!) exclamatory mark before the inline or reference link

Lists

- Bullet Lists
- Numbered Lists
- Mixed Lists
- Blockqoutes
